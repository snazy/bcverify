/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.bcverify.test;

import org.caffinitas.bcverify.BCVerifier;
import org.caffinitas.bcverify.VerifyResult;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class DummyDeveloperTest
{
    @Test
    public void dummy() throws IOException
    {
        BCVerifier verifier = new BCVerifier();

        verifier.addAllowPackages("java");
        verifier.addRejectPackages("java.awt",
            "java.nio", "java.net",
            "java.rmi", "java.security",
            "java.util.jar", "java.util.zip",
            "javax.activation", "javax.imageio",
            "javax.net", "javax.rmi", "javax.swing");
        verifier.addRejectClasses(
            "java.lang.ProcessBuilder",
            "java.lang.Runtime",
            "java.lang.Thread");

        VerifyResult result = verifier.scan(new File("target/classes/"));
        for (Map.Entry<String, VerifyResult.ClassResult> entry : new TreeMap<>(result.getClassResultMap()).entrySet())
        {
            System.out.println(entry.getValue().isAccept());
        }
    }
}
