/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.bcverify;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class VerifyResult
{
    private final Map<String, ClassResult> classResultMap = new HashMap<>();

    private boolean reject;

    public boolean isReject()
    {
        return reject;
    }

    public Map<String, ClassResult> getClassResultMap()
    {
        return Collections.unmodifiableMap(classResultMap);
    }

    void add(ClassResult result)
    {
        classResultMap.put(result.clazz, result);
    }

    public ClassResult checked(String className)
    {
        return classResultMap.get(className);
    }

    public static class ClassResult
    {
        private final String clazz;
        private final boolean accept;
        private String[] reason;

        public ClassResult(String clazz, boolean accept, String reason)
        {
            this.clazz = clazz;
            this.accept = accept;
            this.reason = new String[]{reason};
        }

        public String getClazz()
        {
            return clazz;
        }

        public boolean isAccept()
        {
            return accept;
        }

        public String[] getReason()
        {
            return reason.clone();
        }

        void addReason(String reason)
        {
            String[] r = this.reason;
            r = Arrays.copyOf(r, r.length + 1);
            r[r.length - 1] = reason;
            this.reason = r;
        }

        @Override public String toString()
        {
            return clazz + (accept ? " : ACCEPT" : (" : REJECT : " + Arrays.toString(reason)));
        }
    }
}
