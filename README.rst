Caffinitas Bytecode Verifier
============================

Utility class to check whether the bytecode of a class, classes in an archive (jar) or classes in a class folder
reference only allowed classes.

For example it might be necessary to check that a jar only uses code in the package ``java.*``.
It is also possible to collect the referenced classes.

The "best match" wins. A match is better the longer the pattern that matched is.

Example::

  import org.caffinitas.bcverify.BCVerifier;
  import org.caffinitas.bcverify.VerifyResult;

  void myMethod()
  {
    BCVerifier verifier = new BCVerifier();

    verifier.addAllowPackages("java");                      // allow everything in package java and below
    verifier.addRejectPackages("java.nio", "java.net");     // reject access to network IO
    verifier.addRejectClasses(
            "java.lang.ProcessBuilder",
            "java.lang.Runtime",
            "java.lang.Thread");
    VerifyResult result = verifier.scan(new File("some.jar"));
    for (VerifyResult.ClassResult classResult : new TreeMap<>(result.getClassResultMap()).values())
    {
      System.out.println(  classResult.getClazz()  + " : " + classResult.isAccept());
      if (!classResult.isAccept())
      {
        for (String reason : classResult.getReason())
        {
           // print each rejected usage
           System.out.println("  rejected because: " + reason);
        }
      }
    }
  }

License
=======

Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
